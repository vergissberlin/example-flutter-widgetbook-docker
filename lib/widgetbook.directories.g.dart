// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: unused_import, prefer_relative_imports, directives_ordering

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AppGenerator
// **************************************************************************

import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_example/components/atoms/button.dart';
import 'package:widgetbook_example/components/atoms/custom_card.dart';
import 'package:widgetbook_example/components/atoms/custom_checkbox.dart';
import 'package:widgetbook_example/components/atoms/custom_text_field.dart';
import 'package:widgetbook_example/components/molecules/numpad.dart';

final directories = <WidgetbookNode>[
  WidgetbookFolder(
    name: 'components',
    children: [
      WidgetbookFolder(
        name: 'molecules',
        children: [
          WidgetbookComponent(
            name: 'NumPad',
            useCases: [
              WidgetbookUseCase(
                name: 'Default Style',
                builder: (context) => defaultNumPad(context),
              ),
            ],
          ),
        ],
      ),
      WidgetbookFolder(
        name: 'atoms',
        children: [
          WidgetbookComponent(
            name: 'CustomCheckbox',
            useCases: [
              WidgetbookUseCase(
                name: 'Default Style',
                builder: (context) => defaultCustomCheckbox(context),
              ),
              WidgetbookUseCase(
                name: 'Checked',
                builder: (context) => checkedCustomCheckbox(context),
              ),
            ],
          ),
          WidgetbookComponent(
            name: 'Button',
            useCases: [
              WidgetbookUseCase(
                name: 'Default Style',
                builder: (context) => defaultButton(context),
              ),
              WidgetbookUseCase(
                name: 'With Custom Background Color',
                builder: (context) => customBackgroundButton(context),
              ),
            ],
          ),
          WidgetbookComponent(
            name: 'CustomTextField',
            useCases: [
              WidgetbookUseCase(
                name: 'Default Style',
                builder: (context) => defaultCustomTextField(context),
              ),
              WidgetbookUseCase(
                name: 'With Hint Text',
                builder: (context) => hintTextCustomTextField(context),
              ),
            ],
          ),
          WidgetbookComponent(
            name: 'CustomCard',
            useCases: [
              WidgetbookUseCase(
                name: 'Default Style',
                builder: (context) => defaultCustomCard(context),
              ),
              WidgetbookUseCase(
                name: 'With Custom Background Color',
                builder: (context) => customBackgroundCustomCard(context),
              ),
              WidgetbookUseCase(
                name: 'Yellow Background Color',
                builder: (context) =>
                    anotherCustomBackgroundCustomCard(context),
              ),
              WidgetbookUseCase(
                name: 'With different colors',
                builder: (context) => greenCardUseCase(context),
              ),
            ],
          ),
        ],
      ),
    ],
  ),
];
