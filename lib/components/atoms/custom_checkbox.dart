import 'package:flutter/material.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

class CustomCheckbox extends StatelessWidget {
  const CustomCheckbox({Key? key, required this.value, required this.onChanged})
      : super(key: key);

  final bool value;
  final ValueChanged<bool?> onChanged;

  @override
  Widget build(BuildContext context) {
    return Checkbox(
      value: value,
      onChanged: onChanged,
    );
  }
}

@widgetbook.UseCase(
  name: 'Default Style',
  type: CustomCheckbox,
)
CustomCheckbox defaultCustomCheckbox(BuildContext context) {
  return CustomCheckbox(
    value: context.knobs.boolean(
      label: 'Is active',
      initialValue: true,
    ),
    onChanged: (value) {},
  );
}

@widgetbook.UseCase(
  name: 'Checked',
  type: CustomCheckbox,
)
CustomCheckbox checkedCustomCheckbox(BuildContext context) {
  return CustomCheckbox(
    value: true,
    onChanged: (value) {},
  );
}
