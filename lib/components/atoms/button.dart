import 'package:flutter/material.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

class Button extends StatelessWidget {
  const Button({
    Key? key,
    required this.onPressed,
    required this.child,
    this.style,
  }) : super(key: key);

  final VoidCallback onPressed;
  final Widget child;
  final ButtonStyle? style;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: style,      
      child: child,
    );
  }
}

@widgetbook.UseCase(
  name: 'Default Style',
  type: Button,
)
Button defaultButton(BuildContext context) {
  return Button(
    onPressed: () {}, // Add an empty function to onPressed
    child: const Text('This is a button'), // Add const to Text widget
  );
}

@widgetbook.UseCase(
  name: 'With Custom Background Color',
  type: Button,
)
Button customBackgroundButton(BuildContext context) {
  return Button(
    onPressed: () {}, // Add const to Text widget
    style: ButtonStyle(
      backgroundColor: MaterialStateProperty.all<Color>(
        context.knobs.color(
          label: 'Background color',
          initialValue: Colors.redAccent,
        ),
      ),
    ), // Add an empty function to onPressed
    child: const Text('This is a colorful button'),
  );
}
