import 'package:flutter/material.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart' as widgetbook;

import '../atoms/button.dart';

// Widget with a grid of buttons from 0 to 9 and a delete button
class NumPad extends StatelessWidget {
  const NumPad({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  final ValueChanged<String> onPressed;

  @override
  Widget build(BuildContext context) {
    // set max width and height of the widget
    const maxWidth = 300.0;
    const maxHeight = 400.0;
    return SizedBox(
      width: maxWidth,
      height: maxHeight,
      child: Padding(
        padding: const EdgeInsets.all(12),
        child: GridView.count(
          crossAxisCount: 3,
          shrinkWrap: true,
          primary: false,
          padding: const EdgeInsets.all(20),
          mainAxisSpacing: 12,
          crossAxisSpacing: 12,
          children: [
            for (var i = 1; i < 10; i++)
              Button(
                onPressed: () => onPressed(i.toString()),
                child: Text(i.toString()),
              ),
            Button(
              onPressed: () => onPressed('0'),
              child: const Text('0'),
            ),
            Button(
              onPressed: () => onPressed('delete'),
              child: const Icon(Icons.backspace_outlined),
            ),
          ],
        ),
      ),
    );
  }
}

@widgetbook.UseCase(
  name: 'Default Style',
  type: NumPad,
)
NumPad defaultNumPad(BuildContext context) {
  return NumPad(
    onPressed: (value) {},
  );
}
